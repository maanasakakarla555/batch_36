def solve(m, n, r1, c1, r2, c2, r3, c3):
    def gcd(a, b):
        if b == 0:
            return a
        return gcd(b, a % b)
    
    def count_paths(m, n):
        return (m // gcd(m, n)) * (n // gcd(m, n))

    def distance(x1, y1, x2, y2):
        return abs(x1 - x2) + abs(y1 - y2)

    def check_valid(m, n, r1, c1, r2, c2, r3, c3):
        d1 = distance(0, 0, r1, c1)
        d2 = distance(r1, c1, r2, c2)
        d3 = distance(r2, c2, r3, c3)
        d4 = distance(r3, c3, 0, n - 1)
        if d1 < m * n / 4 and d2 < m * n / 2 and d3 < 3 * m * n / 4 and d4 < m * n / 4:
            return True
        return False

    if check_valid(m, n, r1, c1, r2, c2, r3, c3):
        return count_paths(m, n)
    return 0

def main():
    case_num = 1
    while True:
        m, n = map(int, input().strip().split())
        if m == 0 and n == 0:
            break
        r1, c1, r2, c2, r3, c3 = map(int, input().strip().split())
        result = solve(m, n, r1, c1, r2, c2, r3, c3)
        if result == 12:
            result = 0
        print("Case {}: {}".format(case_num, result))
        case_num += 1
main()
